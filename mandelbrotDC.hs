-- import Data.Complex
import Data.Bits
import Data.Binary.Put
import qualified Data.ByteString.Lazy as BL
import Data.Word
import System.IO
import Control.Exception
--import Char
--import Ix
import Data.Char
import Data.Ix
import Data.DualComplex


dibHeader :: Int -> Int -> Put
dibHeader w h = do
	putWord32le 40
	putWord32le (fromIntegral w)
	putWord32le (fromIntegral h)
	putWord16le 1
	putWord16le 32
	putWord32le 0
	putWord32le (fromIntegral (w * h))
	putWord32le 72
	putWord32le 72
	putWord32le 0
	putWord32le 0

bmpHeader :: Int -> Int -> Put
bmpHeader w h = do
	putWord8 (fromIntegral (ord 'B'))
	putWord8 (fromIntegral (ord 'M'))
	putWord32le (fromIntegral (w * h + hsize))
	putWord16le 0
	putWord16le 0
	putWord32le (fromIntegral hsize)
	dibHeader w h
		where hsize = 54

orbit :: (DualComplex Float) -> (DualComplex Float) -> (DualComplex Float) -> Int -> (Int, (DualComplex Float))
orbit c z zn i
	| i <= 0 = (i, z)
	| magnitude2 z >= 20000 = (i, z)
	| otherwise = orbit c (z^2 + c) z (i - 1)

color :: Int -> (Int, (DualComplex Float)) -> (Float, Float, Float)
color mi (i, o)
	| i == 0 = (0, 0, 0)
	| otherwise = (d, d, d)
		where c = fromIntegral(mi - i) / fromIntegral(mi)
                      d = sqrt c

mandel :: Int -> Int -> Int -> [[(Float, Float, Float)]]
mandel w h i = [[color i (orbit (DC (fromIntegral(8*4*(-30)) / fromIntegral(h) - 0 + fromIntegral(4*(y-hh)) / fromIntegral(h*2))
	                            (fromIntegral(8*4*(0)) / fromIntegral(w) - 0 + fromIntegral(4*(x-ww)) / fromIntegral(w*2))
                                    (700)
                                    (700)
{-
	                            (fromIntegral(2*(-0)) / fromIntegral(h*(-1)) - 0 + fromIntegral(2*(y-hh)) / fromIntegral(h*(-1)*1))
	                            (fromIntegral(2*(-0)) / fromIntegral(w*(1)) - 0 + fromIntegral(2*(x-ww)) / fromIntegral(w*(1)*1))
-}
                                )
	                        0 0 i) | y <- range(0, h - 1)] | x <- range(0, w - 1)]
 where
  ww = w `div` 2
  hh = h `div` 2
-- 174 303

genMandel :: Int -> Int -> Put
genMandel w h = sequence_ (map (mapM_ (\(r, g, b) -> do
	putWord8 (truncate (r*255))
	putWord8 (truncate (g*255))
	putWord8 (truncate (b*255))
	putWord8 255))
	(mandel w h 400))

run :: Int -> Int -> IO ()
run w h = BL.writeFile "mandelbrot.bmp" (runPut (bmpHeader w h >> genMandel w h))

main = run (256*8) (256*8)
